# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
  . /etc/bashrc
fi

# User specific environment
PATH="$HOME/.local/bin:$HOME/bin:$PATH"
export PATH

# -----

# Color variables
RED="\[\033[0;31m\]"
YELLOW="\[\033[0;33m\]"
GREEN="\[\e[36m\]"
COLOR_NONE="\[\e[0m\]"

# -----

# Bash prompt functions
# Print error code
nonzero_return () {
  RETVAL=$?
  [ $RETVAL -ne 0 ] && echo " [$RETVAL] "
}
export -f nonzero_return      # To be able to use it in PS4

# Print git branch
parse_git_branch () {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

# Prompt
export PS1="🐧$RED\$(nonzero_return)$GREEN\u@\h$RED\w$YELLOW\$(parse_git_branch)$COLOR_NONE \\$ "

# Output for set -x
#export PS4="🛠️ $RED\$(nonzero_return)$GREEN${BASH_SOURCE}$RED@$YELLOW${LINENO}$GREEN: $RED\${FUNCNAME[0]}()$COLOR_NONE: "
#export PS4="🛠️ ${BASH_SOURCE}@${LINENO}: ${FUNCNAME[0]:+${FUNCNAME[0]}(): }"
export PS4='🛠️ (${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'

# -----

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# -----

# User specific aliases and functions
# Set default git editor
export VISUAL=vim
export EDITOR="$VISUAL"

# Set explicitly editor for git
#export GIT_EDITOR=vim

# -----

# ls aliases
alias ll='ls -laF'
alias la='ls -A'
alias l='ls -CF'

# ps alias
alias psc='ps xawf -eo pid,user,cgroup,args'

# Colored outputs related aliases
if [ -x /usr/bin/dircolors ]; then
  alias ls='ls --color=auto'
  alias dir='dir --color=auto'
  alias vdir='vdir --color=auto'
  alias grep='grep --color=auto'
  alias fgrep='fgrep --color=auto'
  alias egrep='egrep --color=auto'
fi

# -----

# Smart aliases
# Raspberry Pi alias for getting onboard temperature
rpi-temp () {
  /opt/vc/bin/vcgencmd measure_temp 2>/dev/null
  [ $? -ne "0" ] && echo "Error: This command is hw-specific."
}
export -f rpi-temp

# Open commit with commit message from last commit
# Causing formating issues -> need to restart bash
#git-commit () {
#  git log -1 --pretty=%B | git commit -F - -e
#}
# export -f git-commit

# -----
